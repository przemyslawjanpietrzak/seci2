Compile server

`make server`

Compile client

`make client`

Run server

`./server`

Run client

`./client <ipaddress>`