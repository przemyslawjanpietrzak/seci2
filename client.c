#include <stdio.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdbool.h>

#define PORT "8001"

#define MAXDATASIZE 100
#define MAXNAMESIZE 25

void *receive_handler(void *);
void *get_in_addr(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

bool is_quit(char text[]) {
    return text[0] == '/' && text[1] == 'q' && text[2] == 'u' && text[3] == 'i' && text[4] == 't';
}

bool is_join(char text[]) {
    return text[0] == '/' && text[1] == 'j' && text[2] == 'o' && text[3] == 'i' && text[4] == 'n';
}

void slice_str(char str[], char buffer[]) {
    for (int i = 6; i < strlen(str) - 1; i++) {
        buffer[i - 6] = str[i];
    }
}

void get_room(char text[], char buffer[]) {
    for (int i = 0; i < strlen(text) - 1; i++) {
        if (text[i] == ':') return;
        buffer[i] = text[i];
    }
}

void str_cut(char *str, int begin, int len) {
    int l = strlen(str);

    if (begin + len > l) len = l - begin;
    memmove(str + begin, str + begin + len, l - len + 1);
}

char current_room[] = "default"; 

int main(int argc, char *argv[]) {
    
    int sockfd;
    
    struct addrinfo hints, *servinfo, *result;
    int rv;
    char text[MAXDATASIZE];

    char message[MAXDATASIZE];
    char nickName[MAXNAMESIZE];

    char s[INET6_ADDRSTRLEN];

    if (argc != 2) {
        fprintf(stderr,"usage: client hostname\n");
        exit(1);
    }

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((rv = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0) { // connect with server
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    for(result = servinfo; result != NULL; result = result->ai_next) {
        if ((sockfd = socket(result->ai_family, result->ai_socktype, result->ai_protocol)) == -1) {
            perror("socket error");
            continue;
        }

        if (connect(sockfd, result->ai_addr, result->ai_addrlen) == -1) {
            close(sockfd);
            perror("connection error");
            continue;
        }

        break;
    }

    if (result == NULL) {
        fprintf(stderr, "client: connect failed\n");
        return 2;
    }

    inet_ntop(
        result->ai_family,
        get_in_addr((struct sockaddr *)result->ai_addr),
        s,
        sizeof s
    );
    printf("client is connecting to %s\n", s);

    freeaddrinfo(servinfo);
    
    // set nickname
    puts("Nickname:");
    memset(&nickName, sizeof(nickName), 0);
    memset(&message, sizeof(message), 0);
    fgets(nickName, MAXNAMESIZE, stdin);
    
    // receiving messages thread:
    pthread_t recived_thread;
    
    if(pthread_create(&recived_thread, NULL, receive_handler, (void*)(intptr_t) sockfd) < 0) {
        perror("could not create thread");
        return 1;
    }
    puts("handler");
    puts("Connected\n"); // send message

    for(;;) {
		char temp[6];
		memset(&temp, sizeof(temp), 0);

        memset(&text, sizeof(text), 0); //clean text buffor
        fgets(text, 100, stdin);

		if (is_quit(text)) {
            return 1;
        }

        if (is_join(text)) {
            slice_str(text, current_room); // set room
            printf("joined %s room", current_room);
        }
        
		int count = 0;
        while(count < strlen(current_room)) {
            message[count] = current_room[count];
            count++;
        }
        message[count] = ':';

        count = 0;
        while(count < strlen(current_room) + strlen(nickName) + 1) {
            message[strlen(current_room) + count + 1] = nickName[count];
            count++;
        }
        message[count - 1] = ':';
        
    
        for(int i = 0; i < strlen(text); i++) {
            message[count] = text[i];
            count++;
        }
        message[count] = '\0';

        //Send text
        if(send(sockfd, message, strlen(message), 0) < 0) {
            puts("Send failed");
            return 1;
        }
        memset(&text, sizeof(text), 0);
    }
    
    pthread_join(recived_thread , NULL);
    close(sockfd);

    return 0;
}

void *receive_handler(void *sock_fd) {
	int socket_descrypto = (intptr_t) sock_fd;
    char buffer[MAXDATASIZE];
    int nBytes;
    char message_room[MAXDATASIZE];

    
    for(;;) {
        if ((nBytes = recv(socket_descrypto, buffer, MAXDATASIZE-1, 0)) == -1) {
            perror("error");
            exit(1);
        }
        else {
            buffer[nBytes] = '\0';
        }

        get_room(buffer, message_room);
        if (strcmp(current_room, message_room) == 0) {
            printf("%s", buffer);
        }
        
    }
}
