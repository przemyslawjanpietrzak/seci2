#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>

#define PORT "8001"

void *get_in_addr(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(void) {
    fd_set master;
    fd_set read_fds;  // temporary descriptor
    int max_descriptors_count;

    int socket_listener; 
    int new_socket;
    struct sockaddr_storage remoteaddr; //client address
    socklen_t addrlen;

    char buffor_client[256];
    int bytes;

    char remoteIP[INET6_ADDRSTRLEN];

    int yes=1;
    int i, j, rv;

    struct addrinfo hints, *ai, *result;

    FD_ZERO(&master);
    FD_ZERO(&read_fds);

    // get socket
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((rv = getaddrinfo(NULL, PORT, &hints, &ai)) != 0) {
        fprintf(stderr, "select server: %s\n", gai_strerror(rv));
        exit(1);
    }
    
    for(result = ai; result != NULL; result = result->ai_next) {
        socket_listener = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
        if (socket_listener < 0) { 
            continue;
        }
        
        // check port
        setsockopt(socket_listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

        if (bind(socket_listener, result->ai_addr, result->ai_addrlen) < 0) {
            close(socket_listener);
            continue;
        }

        break;
    }
    if (result == NULL) {
        fprintf(stderr, "connection error\n");
        exit(2);
    }

    freeaddrinfo(ai); // malock memory
    
    puts("binding successful!");
    if (listen(socket_listener, 10) == -1) {
        perror("listen");
        exit(3);
    }
    puts("hearing aid is on, server listening...");
    
    // add the socket_listener to the master set
    FD_SET(socket_listener, &master);

    // keep track of the biggest file descriptor
    max_descriptors_count = socket_listener;

    for(;;) {
        read_fds = master;
        if (select(max_descriptors_count+1, &read_fds, NULL, NULL, NULL) == -1)  {
            perror("select");
            exit(4);
        }

        // get new data to read
        for(i = 0; i <= max_descriptors_count; i++) {
            if (FD_ISSET(i, &read_fds)) { // got data
                if (i == socket_listener) {
                    // new connections
                    addrlen = sizeof remoteaddr;
                    new_socket = accept(
                        socket_listener,
                        (struct sockaddr *)&remoteaddr,
                        &addrlen
                    );

                    if (new_socket == -1) {
                        perror("accepted");
                    } else {
                        FD_SET(new_socket, &master); // check limit
                        if (new_socket > max_descriptors_count) {
                            max_descriptors_count = new_socket;
                        }
                        printf("selectserver: new connection from %s on "
                               "socket %d\n", inet_ntop(remoteaddr.ss_family,
                               get_in_addr((struct sockaddr*)&remoteaddr),
                               remoteIP, INET6_ADDRSTRLEN), new_socket
                        );
                    }
                } else {
                    // get client data
                    if ((bytes = recv(i, buffor_client, sizeof buffor_client, 0)) <= 0) {   
                        if (bytes == 0) { // close connection 
                            printf("select server: socket %d\n", i);
                        } else {
                            perror("socket error \n"); // client closed connection
                        }
                        close(i);
                        FD_CLR(i, &master); // clean up
                    } else {
                        // client data
                        for(j = 0; j <= max_descriptors_count; j++) {
                            // send to all
                            if (FD_ISSET(j, &master) && j != socket_listener && j != i && send(j, buffor_client, bytes, 0) == -1)  {
                                // except the socket_listener and ourselves
                                perror("send");
                            }
                        }
                    }
                }
            }
        }
    }
    
    return 0;
}
